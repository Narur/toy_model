#include <iostream>
#include <string>
#include <vector>
#include <fstream>



float readvalue(std::fstream &f){	//Read the first value in the line
	std::string line;
	std::getline(f,line);
	std::cout << line << std::endl;

	return std::stof(line);

}



int main(int argc, const char* argv[]){
	std::string input_fn,output_fn,LUT_fn;

	if (argc==4){
		input_fn=argv[1];
		output_fn=argv[2]; 
		LUT_fn=argv[3];
	}
	else {
		std::cout << "Wrong number of arguments! Usage: " << argv[0] << " input_file output_file LUT_fn \n"; 
		return 0;
	}

	//open files for in and output
	std::fstream f_input(input_fn, std::ios::in);
	std::ofstream f_output(output_fn);
	std::fstream f_LUT(LUT_fn,std::ios::in);

	//read values and add them to each other
	double in=readvalue(f_input);
	double LUT=readvalue(f_LUT);
	double out=in+LUT;

	//Output
	std::cout << "I calculated some stuff and will write it now: " << out << std::endl;
	f_output << out << std::endl;
	f_output << "I read the following values from in and LUT: " << in << "	" << LUT << std::endl;

  	
  	//Cleanup
  	f_input.close();
	f_LUT.close();
  	f_output.close();

}