# Pull base image.
FROM centos:7.7.1908
 
LABEL maintainer="Lars Hunger <Lars.Hunger.314@gmail.com>"

#Install Packages for project to build and run (in this case this is only the g++ compiler)
RUN yum -y update && yum -y install gcc-c++ && yum clean all

#Copy and build the Toy Model (using the C++11 standard), after this is done the folder /Toy/ has the executable /toy in it
COPY Toy_model.cpp /Toy/
RUN g++ /Toy/Toy_model.cpp -std=c++11 -o /Toy/toy

#Run the toy model 
ENTRYPOINT ["/Toy/toy"]	#If the container is run, it always will execute /Toy/toy ... the toymodel 
CMD ["/data/in.dat","/data/out.dat","/data/LUT.dat"] #If no runtime parameters are given the CMD values will be passed to the entrypoint and the toym will read in files in data/in.dat and /data/LUT.dat and will write results into data/out.dat

#However the input files and output files can also be given with regard to the Docker container filesystem (e.g. for a different mountpoint or sth.)


